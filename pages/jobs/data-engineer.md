title=Data Engineer
description=eyeo is looking for a remote Data Engineer to help improve the entire data-pipeline.

<? include jobs/header ?>

We create software that puts users in control over their online browsing experience. Our products, such as Adblock Plus, Adblock Browser and Flattr, help sustain and grow a fair, open web, because they give users control while providing user-friendly monetization. Our most popular product, Adblock Plus (ABP), is currently used on over 100 million devices.

Our multi-cultural team wants to change the Internet for the better, and you can become an important part of it. We offer a competitive salary and flexible working hours. We trust our employees and believe in their ideas.

eyeo is headquartered in Cologne, Germany with satellite offices in Berlin and Malmö. However, much of our team works remotely — and so can you.

### Job description

Data is insight. As a continually expanding company with desktop products reaching 100 million users, a growing audience on mobile and a newly acquired system named Flattr, we work on the entire data-pipeline from making data sources available, to providing analysis and extracting insights. We are now at a point where we need to streamline the data pipeline so we are searching for an extra pair of hands, eyes and a big brain, to join our Data department.

### Responsibilities

- Monitor and maintain existing data and processes
- Develop and improve data pipelines
- Add your knowledge in computer science to bring our products to a production ready level
- Full stack approach to data: from raw data to various data products (dashboards, reports, studies, etc)
- Find new ways for us to use and emerge information for different projects
- Uphold best practices on how to keep data processes maintainable
- Combine insights from data with a strict privacy approach

### What we expect

- Advanced skills in scripting languages (Python or R)
- Computer science academic background or equivalent professional experience
- Hands-on experience with UNIX-shell
- Experience with Relational Databases (e.g., PostgreSQL)
- Experience in data-pipeline architecture or ETL tasks
- Knowledge about scalable data platforms, containerization, continuous integration
- Machine Learning would be a plus
- Other than the technical requirements above, we need you to be part of the team: bring creativity to the table, have a problem solving attitude, be a team player and last but not least, understand the requirements at hand and explain your solutions.

<? include jobs/footer ?>
