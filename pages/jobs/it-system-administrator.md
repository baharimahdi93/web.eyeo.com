title=IT System Administrator
description=We’re looking for someone based in or willing to relocate to Cologne who can complete our IT Support team and also improve our communication capabilities.

<? include jobs/header ?>

We create software that puts users in control over their online browsing experience. Our products, such as Adblock Plus, Adblock Browser and Flattr, help sustain and grow a fair, open web, because they give users control while providing user-friendly monetization. Our most popular product, Adblock Plus, is currently used on over 100 million devices.

Our multi-cultural team wants to change the Internet for the better, and you can become an important part of it. As a team of nearly 100 colleagues, some working in Germany and others working remotely around the world, it’s crucial that we have efficient means of communication at all times. We’re looking for someone based in or willing to relocate to Cologne who can complete our IT Support team. Someone who can fix, support, plan, improve, work, participate, stream, ticket, design, communicate, set up, operate, joke, show us the error of our ways, well … you get the point.

### Responsibilities

- Own and improve our entire streaming / video chat stack
- Plan and improve conference room setups
- Provide A/V tech support at live events hosted by eyeo
- Ensure every employee, whether remote or local, has a good camera and microphone setup
- General IT -admin and IT -support

### What we expect

- Working experience with online video conferencing solutions
- Experience in hardware and software support, including operating systems
- Interest in audio / video equipment (e.g. Band / DJ / Theater technician)

### Nice to have

- Experience with a wide range of operating systems
- Experience with various printer systems
- Knowledge of video conferencing systems hardware
- Professional experience in audio / video recording / mixing
- Professional experience in audio / video post-production editing
- Knowledge about room acoustics and how they impact conference rooms

<? include jobs/footer ?>
